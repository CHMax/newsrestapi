package ru.chuprin.NewsRestApp.Util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chuprin.NewsRestApp.models.News;
import ru.chuprin.NewsRestApp.models.NewsSource;
import ru.chuprin.NewsRestApp.models.NewsTopic;
import ru.chuprin.NewsRestApp.services.NewsService;
import ru.chuprin.NewsRestApp.services.NewsSourceService;
import ru.chuprin.NewsRestApp.services.NewsTopicService;

@Service
@Transactional
public class FirstStartDBService implements CommandLineRunner {

    private final NewsService newsService;
    private final NewsSourceService newsSourceService;
    private final NewsTopicService newsTopicService;
    @Autowired
    public FirstStartDBService(NewsService newsService, NewsSourceService newsSourceService, NewsTopicService newsTopicService) {
        this.newsService = newsService;
        this.newsSourceService = newsSourceService;
        this.newsTopicService = newsTopicService;
    }

    @Override
    public void run(String... args) throws Exception {

        NewsSource newsSource1 = new NewsSource("irbis.plus");
        NewsSource newsSource2 = new NewsSource("praktika.irbis.plus");
        newsSourceService.save(newsSource1);
        newsSourceService.save(newsSource2);

        NewsTopic newsTopic1 = new NewsTopic("Помощь юр. лицам");
        NewsTopic newsTopic2 = new NewsTopic("Помощь физ. лицам");
        NewsTopic newsTopic3 = new NewsTopic("О нас");
        NewsTopic newsTopic4 = new NewsTopic("Обновления сервиса");
        newsTopicService.save(newsTopic1);
        newsTopicService.save(newsTopic2);
        newsTopicService.save(newsTopic3);
        newsTopicService.save(newsTopic4);

        News news1 = new News("Обновления законодательства в 2022 году", newsSource1, newsTopic1);
        News news2 = new News("Обновления законодательства в 2023 г", newsSource1, newsTopic1);
        News news3 = new News("Рассказываем о том, как обезопасить себя от мошенников", newsSource1, newsTopic2);
        News news4 = new News("Рассказываем о том, как отдыхают наши работники", newsSource1, newsTopic3);
        News news5 = new News("Знакомим с нашими клиентами. Часть 1", newsSource1, newsTopic3);
        News news6 = new News("Знакомим с нашими клиентами. Часть 2", newsSource1, newsTopic3);
        News news7 = new News("Знакомство с сервисом", newsSource2, newsTopic4);
        News news8 = new News("Нововведения во вкладке \"Суды\"", newsSource2, newsTopic4);
        newsService.save(news1);
        newsService.save(news2);
        newsService.save(news3);
        newsService.save(news4);
        newsService.save(news5);
        newsService.save(news6);
        newsService.save(news7);
        newsService.save(news8);
    }
}
