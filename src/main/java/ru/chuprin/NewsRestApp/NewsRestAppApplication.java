package ru.chuprin.NewsRestApp;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.provider.HibernateUtils;
import ru.chuprin.NewsRestApp.models.News;
import ru.chuprin.NewsRestApp.models.NewsSource;
import ru.chuprin.NewsRestApp.models.NewsTopic;

@SpringBootApplication
public class NewsRestAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(NewsRestAppApplication.class, args);

	}
}
