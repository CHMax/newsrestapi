package ru.chuprin.NewsRestApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.chuprin.NewsRestApp.models.News;
import ru.chuprin.NewsRestApp.models.NewsSource;
import ru.chuprin.NewsRestApp.models.NewsTopic;
import ru.chuprin.NewsRestApp.services.NewsService;
import ru.chuprin.NewsRestApp.services.NewsSourceService;
import ru.chuprin.NewsRestApp.services.NewsTopicService;
import java.util.List;

@RestController
@RequestMapping("/api")
public class NewsController {

    private final NewsService newsService;
    private final NewsSourceService newsSourceService;
    private final NewsTopicService newsTopicService;
    @Autowired
    public NewsController(NewsService newsService, NewsSourceService newsSourceService, NewsTopicService newsTopicService) {
        this.newsService = newsService;
        this.newsSourceService = newsSourceService;
        this.newsTopicService = newsTopicService;
    }

    @GetMapping("/news_source")
    public List<NewsSource> getAllNewsSource() {
        return newsSourceService.findAll();
    }

    @GetMapping("/news_topic")
    public List<NewsTopic> getAllNewsTopic() {
        return newsTopicService.findAll();
    }

    @GetMapping("/news")
    public List<News> getAllNews() {
        return newsService.findAll();
    }

    @GetMapping("/news/{offset}/{pageSize}")
    public Page<News> getAllNewsWithPagination(@PathVariable int offset,
                                               @PathVariable int pageSize) {
        return newsService.findAll(PageRequest.of(offset, pageSize));
    }

    @GetMapping("/news_source/{news_source}/{offset}/{pageSize}")
    public List<News> getAllNewsByNewsSourceWithPagination(@PathVariable String news_source,
                                                           @PathVariable int offset,
                                                           @PathVariable int pageSize) {
        return newsService.findAllNewsByNewsSource(news_source, PageRequest.of(offset, pageSize));
    }

    @GetMapping("/news_topic/{news_topic}/{offset}/{pageSize}")
    public List<News> getAllNewsByNewsTopicWithPagination(@PathVariable String news_topic,
                                                          @PathVariable int offset,
                                                          @PathVariable int pageSize) {
        return newsService.findAllNewsByNewsTopic(news_topic, PageRequest.of(offset, pageSize));
    }
}
