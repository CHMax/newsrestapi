package ru.chuprin.NewsRestApp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chuprin.NewsRestApp.models.NewsTopic;

@Repository
public interface NewsTopicRepository extends JpaRepository<NewsTopic, Long> {
}
