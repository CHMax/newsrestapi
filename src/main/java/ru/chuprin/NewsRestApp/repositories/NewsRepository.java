package ru.chuprin.NewsRestApp.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chuprin.NewsRestApp.models.News;
import java.util.List;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
    Page<News> findAll(Pageable pageable);
    List<News> findByNewsSource_NewsSource(String news_source, Pageable pageable);
    List<News> findByNewsTopic_NewsTopic(String news_topic, Pageable pageable);
}
