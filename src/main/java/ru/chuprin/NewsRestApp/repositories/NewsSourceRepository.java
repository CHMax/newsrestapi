package ru.chuprin.NewsRestApp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chuprin.NewsRestApp.models.NewsSource;

@Repository
public interface NewsSourceRepository extends JpaRepository<NewsSource, Long> {
}
