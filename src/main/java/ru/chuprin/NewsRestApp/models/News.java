package ru.chuprin.NewsRestApp.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "News")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "news_title")
    private String newsTitle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "news_source", referencedColumnName = "id")
    private NewsSource newsSource;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "news_topic", referencedColumnName = "id")
    private NewsTopic newsTopic;

    public News() {};

    public News(String newsTitle, NewsSource newsSource, NewsTopic newsTopic) {
        this.newsTitle = newsTitle;
        this.newsSource = newsSource;
        this.newsTopic = newsTopic;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public void setNewsSource(NewsSource newsSource) {
        this.newsSource = newsSource;
    }

    public void setNewsTopic(NewsTopic newsTopic) {
        this.newsTopic = newsTopic;
    }

    public String getNewsSource() {
        return newsSource.getNewsSource();
    }

    public String getNewsTopic() {
        return newsTopic.getNewsTopic();
    }
}
