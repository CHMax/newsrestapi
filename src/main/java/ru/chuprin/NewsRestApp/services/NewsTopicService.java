package ru.chuprin.NewsRestApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chuprin.NewsRestApp.models.NewsSource;
import ru.chuprin.NewsRestApp.models.NewsTopic;
import ru.chuprin.NewsRestApp.repositories.NewsTopicRepository;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class NewsTopicService {

    private final NewsTopicRepository newsTopicRepository;
    @Autowired
    public NewsTopicService(NewsTopicRepository newsTopicRepository) {
        this.newsTopicRepository = newsTopicRepository;
    }

    public List<NewsTopic> findAll() {
        return newsTopicRepository.findAll();
    }

    @Transactional
    public void save(NewsTopic newsTopic) {
        newsTopicRepository.save(newsTopic);
    }
}
