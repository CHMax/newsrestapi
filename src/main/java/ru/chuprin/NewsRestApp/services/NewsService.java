package ru.chuprin.NewsRestApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chuprin.NewsRestApp.models.News;
import ru.chuprin.NewsRestApp.repositories.NewsRepository;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class NewsService {

    private final NewsRepository newsRepository;
    @Autowired
    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public List<News> findAll() {
        return newsRepository.findAll();
    }

    public Page<News> findAll(Pageable pageable) {
        return newsRepository.findAll(pageable);
    }

    public List<News> findAllNewsByNewsSource(String news_source, Pageable pageable) {
        return newsRepository.findByNewsSource_NewsSource(news_source, pageable);
    }

    public List<News> findAllNewsByNewsTopic(String news_topic, Pageable pageable) {
        return newsRepository.findByNewsTopic_NewsTopic(news_topic, pageable);
    }

    @Transactional
    public void save(News news) {
        newsRepository.save(news);
    }
}
