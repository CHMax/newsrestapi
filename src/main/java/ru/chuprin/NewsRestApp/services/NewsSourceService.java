package ru.chuprin.NewsRestApp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chuprin.NewsRestApp.models.News;
import ru.chuprin.NewsRestApp.models.NewsSource;
import ru.chuprin.NewsRestApp.repositories.NewsSourceRepository;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class NewsSourceService {

    private final NewsSourceRepository newsSourceRepository;
    @Autowired
    public NewsSourceService(NewsSourceRepository newsSourceRepository) {
        this.newsSourceRepository = newsSourceRepository;
    }

    public List<NewsSource> findAll() {
        return newsSourceRepository.findAll();
    }

    @Transactional
    public void save(NewsSource newsSource) {
        newsSourceRepository.save(newsSource);
    }
}
