RUN mvn clean package -DskipTests
FROM openjdk:17-jdk-alpine
ADD target/*.jar news-api.jar
ENTRYPOINT ["java", "-jar", "news-api.jar"]